package com.automationanywhere.botcommand.sk;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.BotCommand;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.model.record.Record;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import java.lang.ClassCastException;
import java.lang.Deprecated;
import java.lang.Double;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class ShowChartCommand implements BotCommand {
  private static final Logger logger = LogManager.getLogger(ShowChartCommand.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  @Deprecated
  public Optional<Value> execute(Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return execute(null, parameters, sessionMap);
  }

  public Optional<Value> execute(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    logger.traceEntry(() -> parameters != null ? parameters.toString() : null, ()-> sessionMap != null ?sessionMap.toString() : null);
    ShowChart command = new ShowChart();
    HashMap<String, Object> convertedParameters = new HashMap<String, Object>();
    if(parameters.containsKey("record1") && parameters.get("record1") != null && parameters.get("record1").get() != null) {
      convertedParameters.put("record1", parameters.get("record1").get());
      if(!(convertedParameters.get("record1") instanceof Record)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","record1", "Record", parameters.get("record1").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("record1") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","record1"));
    }

    if(parameters.containsKey("record2") && parameters.get("record2") != null && parameters.get("record2").get() != null) {
      convertedParameters.put("record2", parameters.get("record2").get());
      if(!(convertedParameters.get("record2") instanceof Record)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","record2", "Record", parameters.get("record2").get().getClass().getSimpleName()));
      }
    }

    if(parameters.containsKey("charttype") && parameters.get("charttype") != null && parameters.get("charttype").get() != null) {
      convertedParameters.put("charttype", parameters.get("charttype").get());
      if(!(convertedParameters.get("charttype") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","charttype", "String", parameters.get("charttype").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("charttype") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","charttype"));
    }
    if(convertedParameters.get("charttype") != null) {
      switch((String)convertedParameters.get("charttype")) {
        case "line" : {

        } break;
        case "pie" : {

        } break;
        case "bar" : {

        } break;
        default : throw new BotCommandException(MESSAGES_GENERIC.getString("generic.InvalidOption","charttype"));
      }
    }

    if(parameters.containsKey("chartlabel") && parameters.get("chartlabel") != null && parameters.get("chartlabel").get() != null) {
      convertedParameters.put("chartlabel", parameters.get("chartlabel").get());
      if(!(convertedParameters.get("chartlabel") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","chartlabel", "String", parameters.get("chartlabel").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("chartlabel") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","chartlabel"));
    }

    if(parameters.containsKey("xlabel") && parameters.get("xlabel") != null && parameters.get("xlabel").get() != null) {
      convertedParameters.put("xlabel", parameters.get("xlabel").get());
      if(!(convertedParameters.get("xlabel") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","xlabel", "String", parameters.get("xlabel").get().getClass().getSimpleName()));
      }
    }

    if(parameters.containsKey("ylabel") && parameters.get("ylabel") != null && parameters.get("ylabel").get() != null) {
      convertedParameters.put("ylabel", parameters.get("ylabel").get());
      if(!(convertedParameters.get("ylabel") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","ylabel", "String", parameters.get("ylabel").get().getClass().getSimpleName()));
      }
    }

    if(parameters.containsKey("width") && parameters.get("width") != null && parameters.get("width").get() != null) {
      convertedParameters.put("width", parameters.get("width").get());
      if(!(convertedParameters.get("width") instanceof Double)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","width", "Double", parameters.get("width").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("width") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","width"));
    }

    if(parameters.containsKey("height") && parameters.get("height") != null && parameters.get("height").get() != null) {
      convertedParameters.put("height", parameters.get("height").get());
      if(!(convertedParameters.get("height") instanceof Double)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","height", "Double", parameters.get("height").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("height") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","height"));
    }

    try {
      command.action((Record)convertedParameters.get("record1"),(Record)convertedParameters.get("record2"),(String)convertedParameters.get("charttype"),(String)convertedParameters.get("chartlabel"),(String)convertedParameters.get("xlabel"),(String)convertedParameters.get("ylabel"),(Double)convertedParameters.get("width"),(Double)convertedParameters.get("height"));Optional<Value> result = Optional.empty();
      return logger.traceExit(result);
    }
    catch (ClassCastException e) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.IllegalParameters","action"));
    }
    catch (BotCommandException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }
}
