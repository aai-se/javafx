package com.automationanywhere.botcommand.sk;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.BotCommand;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import java.lang.ClassCastException;
import java.lang.Deprecated;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class TwoButtonsCommand implements BotCommand {
  private static final Logger logger = LogManager.getLogger(TwoButtonsCommand.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  @Deprecated
  public Optional<Value> execute(Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return execute(null, parameters, sessionMap);
  }

  public Optional<Value> execute(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    logger.traceEntry(() -> parameters != null ? parameters.toString() : null, ()-> sessionMap != null ?sessionMap.toString() : null);
    TwoButtons command = new TwoButtons();
    HashMap<String, Object> convertedParameters = new HashMap<String, Object>();
    if(parameters.containsKey("title") && parameters.get("title") != null && parameters.get("title").get() != null) {
      convertedParameters.put("title", parameters.get("title").get());
      if(!(convertedParameters.get("title") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","title", "String", parameters.get("title").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("title") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","title"));
    }

    if(parameters.containsKey("label") && parameters.get("label") != null && parameters.get("label").get() != null) {
      convertedParameters.put("label", parameters.get("label").get());
      if(!(convertedParameters.get("label") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","label", "String", parameters.get("label").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("label") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","label"));
    }

    if(parameters.containsKey("button1") && parameters.get("button1") != null && parameters.get("button1").get() != null) {
      convertedParameters.put("button1", parameters.get("button1").get());
      if(!(convertedParameters.get("button1") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","button1", "String", parameters.get("button1").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("button1") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","button1"));
    }

    if(parameters.containsKey("button2") && parameters.get("button2") != null && parameters.get("button2").get() != null) {
      convertedParameters.put("button2", parameters.get("button2").get());
      if(!(convertedParameters.get("button2") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","button2", "String", parameters.get("button2").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("button2") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","button2"));
    }

    try {
      Optional<Value> result =  Optional.ofNullable(command.action((String)convertedParameters.get("title"),(String)convertedParameters.get("label"),(String)convertedParameters.get("button1"),(String)convertedParameters.get("button2")));
      return logger.traceExit(result);
    }
    catch (ClassCastException e) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.IllegalParameters","action"));
    }
    catch (BotCommandException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }
}
